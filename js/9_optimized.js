
function calculate(x) {
    return new Promise((resolve, reject) => {
        if (x == 1) resolve(true); 
        for (let i=2; i<=x/2; i++) {
            if (x % i == 0) {
                resolve(false);
        	}
        }
        resolve(true);
    })
}

(async function doCalculation() {
	let resultsPromises = []
	for (let i=0; i<20; i++) {
	    resultsPromises.push(calculate(i))
	}
	let results = await Promise.all(resultsPromises).then(values => values)
	results.map((val, index) => console.log(`${index} ${val}`))
})()

