function doSomeCalculation(num) {
	const result = num**2;
    return new Promise((resolve, reject) => {
		setTimeout(function(){ resolve(result) }, 3000);
	})
}

(async function execute() {
	await doSomeCalculation(5).then(function (result) { 
		console.log(`Result ${result}`)
	})
})()