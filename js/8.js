function doSomeCalculation(num, callback) {
	const result = num**2
    setTimeout(function(){ callback(result) }, 3000);
}

(async function execute() {
	doSomeCalculation(5, function(result) {
		console.log(`Result ${result}`)
	})
})()