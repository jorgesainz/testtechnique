
function calculate(x) {
    return new Promise((resolve, reject) => {
        if (x == 1) resolve(true); 
        for (let i=2; i<x; i++) {
            if (x % i == 0) {
                resolve(false);
        	}
        }
        resolve(true);
    })
}

(async function doCalculation() {
	let result
	for (let i=0; i<20; i++) {
	    result = await calculate(i)
		console.log(`${i} ${result}`)
	}
})()

