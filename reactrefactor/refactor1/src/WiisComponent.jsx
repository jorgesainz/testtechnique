import React from 'react'

class WiisComponent extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			name: "",
			email: ""
		}
	
		this.handleNameChange = this.handleNameChange.bind(this);
		this.handleEmailChange = this.handleEmailChange.bind(this);
		this.handleOnshowNameInformation = this.handleOnshowNameInformation.bind(this);
		this.handleOnshowEmailInformation = this.handleOnshowEmailInformation.bind(this);
		this.handleResetAction = this.handleResetAction.bind(this);
		this.handleClearName = this.handleClearName.bind(this);
		this.handleClearEmail = this.handleClearEmail.bind(this);
	}

	handleOnshowNameInformation() {
		alert(`Name information : ${this.state.name}`)
	}

	handleOnshowEmailInformation() {
		alert(`Email information : ${this.state.email}`)
	}	

	handleResetAction() {
		this.setState({name: ""});
		this.setState({email: ""});
	}
	
	handleClearName() {
		this.setState({name: ""});
	}
	
	handleNameChange(event) {
		this.setState({name: event.target.value});
	}
	
	handleClearEmail() {
		this.setState({email: ""});
	}
		
	handleEmailChange(event) {
		this.setState({email: event.target.value});
	}

    render() {
		return (
			<div>
				<div>
					<button onClick={this.handleOnshowNameInformation} className="wiisbutton" >Show Name Information</button>
					<button onClick={this.handleOnshowEmailInformation} className="wiisbutton" >Show Email Information</button>
					<button onClick={this.handleResetAction} className="wiisbutton" >Reset</button>
				</div>
				<div>
					<label>
						Name:
						<input type="text" value={this.state.name} onChange={this.handleNameChange} />
						<button onClick={this.handleClearName} >Clear</button>
					</label>
				</div>
				<div>
					<label>
						Email:
						<input type="text" value={this.state.email} onChange={this.handleEmailChange} />
						<button onClick={this.handleClearEmail} >Clear</button>
					</label>
				</div>	
			</div>
		)
	}
}

export default WiisComponent;