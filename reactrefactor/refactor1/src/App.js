import React from 'react';
import './App.css';
import WiisComponent from './WiisComponent'

function App() {	
  return (
    <div className="App">
		<h1> Wiis test </h1>
		<WiisComponent />
    </div>
  );
}

export default App;
