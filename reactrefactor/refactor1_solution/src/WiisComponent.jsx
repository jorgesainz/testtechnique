import React from 'react'

class WiisInfoButton extends React.Component {
	constructor(props) {
		super(props)
		this.showInfo = this.showInfo.bind(this);
	}
	
	showInfo() {
		alert(`${this.props.info} Information : ${this.props.value}`)
	}
			
    render() {
		return (
			<button onClick={this.showInfo} className="wiisbutton" >Show {this.props.info} Information</button>
		)
	}
}

class WiisInput extends React.Component {
	constructor(props) {
		super(props)
		this.handleClear = this.handleClear.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	handleClear() {
		this.props.onNewValue(this.props.keyvalue, "")
	}
	
	handleChange(event) {
		this.props.onNewValue(this.props.keyvalue, event.target.value)
	}
		
    render() {
		return (
			<div>
				<label>
					{this.props.info} :
					<input type="text" value={this.props.value} onChange={this.handleChange} />
					<button onClick={this.handleClear} >Clear</button>
				</label>
			</div>	
		)
	}
}

class WiisComponent extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			name: "",
			email: ""
		}

		this.onNewInfoValue = this.onNewInfoValue.bind(this);
		this.handleResetAction = this.handleResetAction.bind(this);
	}

	handleResetAction() {
		this.setState({name: "", email: ""});
	}

	onNewInfoValue(key, value) {
		switch(key) {
			case "name":
				this.setState({name: value});
				break;
			case "email":
				this.setState({email: value});
				break;
			default: /* just to make some checkers happy */
				console.log(`${key} not found`)
		}
	}

    render() {
		return (
			<div>
				<div>
					<WiisInfoButton info="Name" value={this.state.name} />
					<WiisInfoButton info="Email" value={this.state.email} />
					<button onClick={this.handleResetAction} className="wiisbutton" >Reset</button>
				</div>
				<WiisInput onNewValue={this.onNewInfoValue} info="Name" keyvalue="name" value={this.state.name} />
				<WiisInput onNewValue={this.onNewInfoValue} info="Email" keyvalue="email" value={this.state.email} 	/>
			</div>
		)
	}
}

export default WiisComponent;