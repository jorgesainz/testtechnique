const app = require('./app.js')
const request = require('supertest')
const sinon = require('sinon')
var UserController = require('./userController.js')
var fs = require('fs');
var assert = require('assert');
const { kMaxLength } = require('buffer');

describe('Testing users APi - GET', () => {

    afterEach(async () => {
        sinon.restore()
    })

    it('get error if no file defined', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return null
        })
        await request(app)
            .get('/users/1')
            .expect(404)
    })

    it('get error if file cannot be read', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return "./myNoExistingFile.json"
        })
        await request(app)
            .get('/users/1')
            .expect(404)
    })

    it('get asked user', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return "./testFile.json"
        })
        var usersData = [{
            id:1,
            name:'test1',
            login: 'test1login'
        },
        {
            id: 2,
            name:'test2',
            login: 'test2login'
        }]
		fs.writeFileSync('./testFile.json', JSON.stringify(usersData))
        const user = await request(app)
            .get('/users/2')
            .expect(200)
            .then((res) => res.body)

	    assert.equal(user.name, 'test2')
        assert.equal(user.login, 'test2login')

	    fs.unlink('./testFile.json', () => {})
    })

	it('get 204 if requested for a non existing user', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return "./testFile.json"
        })
        var usersData = [{
            id:1,
            name:'test1',
            login: 'test1login'
        }]
		fs.writeFileSync('./testFile.json', JSON.stringify(usersData))
        const users = await request(app)
            .get('/users/2')
            .expect(204)
            .then((res) => res.body)

	    fs.unlink('./testFile.json', () => {})
    })
})


describe('Testing users APi - POST', () => {

    afterEach(async () => {
        sinon.restore()
    })

    const path = "./testFile.json"
    it('creates file if not existing file', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return path
        })
        assert.notEqual(fs.existsSync(path), true)

        const users = await request(app)
            .post('/users')
            .send({ name: "newName", login: "newname" })
            .expect(201)
            .then((res) => res.body)

        assert.equal(fs.existsSync(path), true)
        fs.unlink('./testFile.json', () => {})
    })

    it('Add user if login doesnt exist', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return './testFile.json'
        })

        const users = await request(app)
            .post('/users')
            .send({ name: "newName", login: "newname" })
            .expect(201)
            .then((res) => res.body)

        const fileContents = fs.readFileSync('./testFile.json')
        var parsedFile = JSON.parse(fileContents)
        var insertedUser = parsedFile.find((element) => {
            return element.name === "newName" &&
                element.login === "newname"
        })

        assert.notEqual(insertedUser, null)

        fs.unlink('./testFile.json', () => {})
    })

    it('do not insert user if already existing login ', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return './testFile.json'
        })
        var usersData = [{
            id:1,
            name:'test1',
            login: 'test1login'
        }]
        fs.writeFileSync('./testFile.json', JSON.stringify(usersData))

        await request(app)
            .post('/users')
            .send({ name: "test7", login: "test1login" })
            .expect(409)
            .then((res) => res.body)

        const fileContents = fs.readFileSync('./testFile.json')
        var parsedFile = JSON.parse(fileContents)
        assert.equal(parsedFile.length, 1)

        fs.unlink('./testFile.json', () => {})
    })

    it('can insert the name twice if login is different', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return './testFile.json'
        })
        var usersData = [{
            id:1,
            name:'test1',
            login: 'test1login'
        }]
        fs.writeFileSync('./testFile.json', JSON.stringify(usersData))

        await request(app)
            .post('/users')
            .send({ name: "test1", login: "testdifferentlogin" })
            .expect(201)
            .then((res) => res.body)

        const fileContents = fs.readFileSync('./testFile.json')
        var parsedFile = JSON.parse(fileContents)
        assert.equal(parsedFile.length, 2)

        var insertedUser = parsedFile.find((element) => {
            return element.name === "test1" &&
                element.login === "testdifferentlogin"
        })

        assert.notEqual(insertedUser, null)

        fs.unlink('./testFile.json', () => {})
    })
})

describe('Testing users APi - UPDATE', () => {

    afterEach(async () => {
        sinon.restore()
    })

    it('get error if file doesnt exist', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return "./myNoExistingFile.json"
        })
        fs.writeFileSync('./testFile.json', JSON.stringify([]))

        await request(app)
            .put('/users/1')
            .send({ name: "test1", login: "testdifferentlogin" })
            .expect(404)
    })

    it('get error if user cannot be found to be updated ', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return "./testFile.json"
        })
        fs.writeFileSync('./testFile.json', JSON.stringify([]))

        await request(app)
            .put('/users/1')
            .send({ name1: "test1", login1: "testdifferentlogin" })
            .expect(404)

        fs.unlink('./testFile.json', () => {})
    })

    it('error if key of elements of user object are not correct', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return './testFile.json'
        })
        fs.writeFileSync('./testFile.json', JSON.stringify([]))

        await request(app)
            .put('/users')
            .send({ name1: "test1", login: "testdifferentlogin" })
            .expect(404)
            .then((res) => res.body)
        await request(app)
            .put('/users')
            .send({ name: "test1", login1: "testdifferentlogin" })
            .expect(404)
            .then((res) => res.body)

        const fileContents = fs.readFileSync('./testFile.json')
        assert.equal(fileContents.toString(), "[]")

        fs.unlink('./testFile.json', () => {})
    })

	it('error if element to be updated cannot be found', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return './testFile.json'
        })
        fs.writeFileSync('./testFile.json', JSON.stringify([]))

        var usersData = [{
            id:1,
            name:'test1',
            login: 'test1login'
        }]
        fs.writeFileSync('./testFile.json', JSON.stringify(usersData))

        await request(app)
            .put('/users')
            .send({ name: "test2", login: "testdifferentlogin" })
            .expect(404)
            .then((res) => res.body)

        const fileContents = fs.readFileSync('./testFile.json')
        var parsedFile = JSON.parse(fileContents)
        assert.equal(parsedFile.length, 1)

        var insertedUser = parsedFile.find((element) => {
            return element.name === "test1" &&
                element.login === "test1login"
        })

        assert.notEqual(insertedUser, null)

        fs.unlink('./testFile.json', () => {})
    })

	it('update user on file', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return './testFile.json'
        })
        var usersData = [{
            id:1,
            name:'test1',
            login: 'test1login'
        }]
        fs.writeFileSync('./testFile.json', JSON.stringify(usersData))

        await request(app)
            .post('/users')
            .send({ name: "test1", login: "testdifferentlogin" })
            .expect(201)
            .then((res) => res.body)

        const fileContents = fs.readFileSync('./testFile.json')
        var parsedFile = JSON.parse(fileContents)
        assert.equal(parsedFile.length, 2)

        var insertedUser = parsedFile.find((element) => {
            return element.name === "test1" &&
                element.login === "testdifferentlogin"
        })

        assert.notEqual(insertedUser, null)

        fs.unlink('./testFile.json', () => {})
    })

})


describe('Testing users APi - DELETE', () => {

    afterEach(async () => {
        sinon.restore()
    })

    it('get error if no file defined', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return null
        })

        await request(app)
            .delete('/users/1')
            .expect(404)
            .then((res) => res.body)
    })

	it('delete user from file', async () => {
        sinon.stub(UserController, 'db').callsFake(() => {
            return './testFile.json'
        })
        var usersData = [{
            id:1,
            name:'test1',
            login: 'test1login'
        },
        {
            id:2,
            name:'test2',
            login: 'test2login'
        }]
        fs.writeFileSync('./testFile.json', JSON.stringify(usersData))

        await request(app)
            .delete('/users/1')
            .expect(200)
            .then((res) => res.body)

        const fileContents = fs.readFileSync('./testFile.json')
        var parsedFile = JSON.parse(fileContents)
        assert.equal(parsedFile.length, 1)
        var existingUser = parsedFile.find((element) => {
            return element.name === "test2" &&
                element.login === "test2login"
        })
        assert.notEqual(existingUser, null)

        fs.unlink('./testFile.json', () => {})
    })

})