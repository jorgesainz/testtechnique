var fs = require('fs')

class UserController {
    static db() {
        return "./file.json"
    }

    static user(req, res) {
        const dbFile = UserController.db()
        if(!dbFile) {
              res.status(404).send(null)
			  return
        }
		const idToSearch = parseInt(req.params.id)
		var fileContents = null
        try {
			fileContents = fs.readFileSync(dbFile)
		} catch (e) {
			res.status(404).send(null)
			return
		}
        var parsedFile = JSON.parse(fileContents)
		var found = null
        res.status(200).send(found)
	}

    static update(req, res) {

    }

    static delete(req, res) {

    }

    static create(req, res) {

    }
}

module.exports = UserController
