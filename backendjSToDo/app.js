var express = require('express')
var http = require('http-server')
var bodyParser = require('body-parser')
var UserController = require('./userController.js')

const app = express()

app.use(bodyParser.json())

app.get('/users/:id', UserController.user)
app.put('/users/:id', UserController.update)
app.delete('/users/:id', UserController.delete)
app.post('/users', UserController.create)

const server = app.listen(9876, () => {
    console.log("Server started")
})

module.exports = server
