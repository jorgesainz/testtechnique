var fs = require('fs')

class UserController {
    static db() {
        return "./file.json"
    }

    static user(req, res) {
        const dbFile = UserController.db()
        if(!dbFile) {
              res.status(404).send(null)
			  return
        }
		const idToSearch = parseInt(req.params.id)
		var fileContents = null
        try {
			fileContents = fs.readFileSync(dbFile)
		} catch (e) {
			res.status(404).send(null)
			return
		}
        var parsedFile = JSON.parse(fileContents)
		var found = parsedFile.find((element) => {
		    return element.id === idToSearch
		})
		if(!found) {
			res.status(204).send(null)
			return
		}
        res.status(200).send(found)
	}

    static update(req, res) {
		const dbFile = UserController.db()
        if(!dbFile) {
              res.status(404).send(null)
			  return
        }
		const idToSearch = parseInt(req.params.id)
		const {name, login} = req.body
		var fileContents = null
        try {
			fileContents = fs.readFileSync(dbFile)
		} catch (e) {
			res.status(404).send(null)
			return
		}
        var parsedFile = JSON.parse(fileContents)
		var foundIndex = parsedFile.findIndex((element) => {
		    return element.id === idToSearch
		})
		if(foundIndex === -1) {
			res.status(404).send(null)
			return
		}
		parsedFile[foundIndex].name = name
		parsedFile[foundIndex].login = login
		fs.writeFileSync(dbFile, JSON.stringify(parsedFile))
        res.status(200).send(found)
    }

    static delete(req, res) {
        const dbFile = UserController.db()
        if(!dbFile) {
              res.status(404).send(null)
			  return
        }
		const idToSearch = parseInt(req.params.id)
		var fileContents = []
        try {
			fileContents = fs.readFileSync(dbFile)
		} catch (e) {
			res.status(404).send(null)
			return
		}

		var parsedFile = JSON.parse(fileContents)
		var foundIndex = parsedFile.findIndex((element) => {
			console.log("ckomparing ", element, idToSearch)
			return element.id === idToSearch
		})
		if(foundIndex === -1) {
			res.status(204).send(null)
			return
		}
		parsedFile.splice(foundIndex, 1);
		fs.writeFileSync('./testFile.json', JSON.stringify(parsedFile))
        res.status(200).send(null)
    }

    static create(req, res) {
		const dbFile = UserController.db()
		const idToSearch = parseInt(req.params.id)
		const {name, login} = req.body
		var fileContents = '[]'
        try {
			fileContents = fs.readFileSync(dbFile)
		} catch (e) {
			//console.log(e)
		}
		var parsedFile = JSON.parse(fileContents)
		var foundIndex = parsedFile.findIndex((element) => {
		    return element.login === login
		})
		if (foundIndex !== -1) {
			res.status(409).send(null)
			return
		}
		let biggestId = 0
		parsedFile.forEach(element => {
			if (element.id > biggestId) {
				biggestId = element.id
			}
		});
		const newId = biggestId+1
		const newElement = {id: newId, name: name, login:login}
		parsedFile.push(newElement)
		fs.writeFileSync(dbFile, JSON.stringify(parsedFile))
        res.status(201).send(newElement)
    }
}

module.exports = UserController
